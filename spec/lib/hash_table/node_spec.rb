# frozen_string_literal: true

require 'spec_helper'

RSpec.describe Node do
  describe '#initialize' do
    let(:key) { 'key' }
    let(:value) { 'value' }
    let(:next_node) { Node.new('another-key', 'another-value') }

    subject { described_class.new(key, value, next_node) }

    it { is_expected.to be }
  end
end
