# frozen_string_literal: true

require 'spec_helper'

RSpec.describe List do
  describe 'add' do
    let(:list) { described_class.new }
    let(:key1) { 'key1' }
    let(:value1) { 'val1' }

    subject(:add_node) { list.add(key1, value1) }

    it 'returns added node' do
      expect(add_node).to be_instance_of Node
    end

    context 'when adding multiple nodes' do
      before do
        add_node
        list.add('key2', 'value2')
        list.add('key3', 'value3')
      end

      specify do
        expect(list).to be
      end
    end
  end
end
