# frozen_string_literal: true

require 'spec_helper'

RSpec.describe HashTable do
  subject(:table) { described_class.new }

  it { expect(described_class).not_to be_kind_of(Hash) }
  it { expect(described_class.ancestors).not_to include(Hash) }

  describe '#[]=' do
    it { expect { table['foo'] = 'bar' }.not_to raise_error }
    it { expect(table['foo'] = 'bar').to eq('bar') }
    it { expect { table[:foo] = 'bar' }.not_to raise_error }
    it { expect(table[:foo] = 'bar').to eq('bar') }
    it { expect { table[555] = 'bar' }.not_to raise_error }
    it { expect(table[555] = 'bar').to eq('bar') }

    context 'benchmark' do
      it { expect { table[:add] = :me }.to perform_at_least(100_000).ips }
    end
  end

  describe '#[]' do
    before do
      table['foo'] = 'bar'
      table[:foo] = 123
    end

    it { expect(table['foo']).to eq('bar') }
    it { expect(table[:foo]).to eq(123) }
    it { expect(table[:unknown]).to be_nil }

    context 'benchmark' do
      before do
        1_000.times do |i|
          table[i] = :integer_as_value
          table[i.to_s] = :string_as_value
          table[:"#{i}"] = :symbol_as_value
        end
      end

      it { expect { table[:not_included] }.to perform_at_least(100_000).ips }
    end
  end
end
