# frozen_string_literal: true

class List
  include ::Enumerable

  attr_reader :head

  def add(key, value)
    return set_head(key, value) unless head
    node = head
    node = node.next_node while node.next?
    new_node = Node.new(key, value)
    node.next_node = new_node
    new_node
  end

  def each
    return unless head
    yield head
    current = head
    while current.next?
      yield current
      current = current.next_node
    end
  end

  private

  def set_head(key, value)
    @head ||= Node.new(key, value)
  end
end
