# frozen_string_literal: true

class HashTable
  DEFAULT_BIN_SIZE = 16

  def initialize
    @bin_size = DEFAULT_BIN_SIZE
    @density = 10
    @bins = Array.new(bin_size) { List.new }
    @size = 0
  end

  def [](key)
    find(key)
  end

  def []=(key, value)
    add(key, value)
  end

  private

  attr_reader :bin_size, :bins, :size, :density

  def find(key)
    find_node(key)&.value
  end

  def find_node(key)
    bin_for(key).find do |node|
      key == node.key
    end
  end

  def add(key, value)
    node = find_node(key)

    if node
      node.value = value
    else
      resize if full?
      bin_for(key).add(key, value)
      @size += 1
    end
    value
  end

  def bin_for(key)
    bins[bin_index(key)]
  end

  def bin_index(key)
    key.hash % bin_size
  end

  def resize
    resize_bin_size
    new_bins = Array.new(@bin_size) { List.new }

    bins.each do |bin|
      bin.each do |node|
        new_bins[bin_index(node.key)].add(node.key, node.value)
      end
    end

    @bins = new_bins
  end

  def resize_bin_size
    @bin_size = bin_size ** 2
  end

  def full?
    size > density * bin_size
  end
end
