#### Summary

Implementation of a hash table in Ruby

#### Requirements

- ruby 2.5.x

#### Setup

- gem install 'bundler'
- bundle


#### Run

- rspec

or in irb/pry

```
=> require './lib/hash_table'
=> h = HashTable.new
=> h[:key] = '123'
=> "123"
=> pry(main)> h[:key]
=> "123"
```


### Benchmark

```
> ruby benchmark.rb 100_000

                 user     system      total        real
HashTable    0.326411   0.008171   0.334582 (  0.335579)
MRI Hash     0.072153   0.003302   0.075455 (  0.076262)
```