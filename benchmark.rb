# frozen_string_literal: true

require 'securerandom'
require 'benchmark'
require_relative 'lib/hash_table'

class HashTable::Benchmark
  SAMPLE = Struct.new(:key, :value)

  def initialize(iterations_count)
    @items = Array.new(iterations_count) { sample_item }
  end

  def report
    Benchmark.bm(10) do |x|
      x.report('HashTable') { test(HashTable.new) }
      x.report('MRI Hash')  { test({}) }
    end
  end

  private

  attr_reader :items

  def sample_item
    rand = SecureRandom.uuid
    SAMPLE.new(rand[0..4], rand)
  end

  def test(testable)
    write(testable)
    read(testable)
  end

  def read(testable)
    items.each { |sample| testable[sample.key] }
  end

  def write(testable)
    items.each do |sample|
      testable[sample.key] = sample.value
    end
  end
end

HashTable::Benchmark.new(ARGV.first&.to_i || 100_000).report